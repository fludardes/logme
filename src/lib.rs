#[allow(unused_imports)]
#[macro_use]
extern crate log;
extern crate chrono;

use log::{LevelFilter, SetLoggerError};
use logger::Logger;

static LOGGER: Logger = Logger;

pub fn init() -> Result<(), SetLoggerError> {
    log::set_max_level(LevelFilter::Trace);
    log::set_logger(&LOGGER)
}

#[cfg(test)]
mod tests {
    use super::init;

    #[test]
    fn it_works() {
        init().expect("Failed to init logger");
        info!("Hello");
    }

}

mod logger;
