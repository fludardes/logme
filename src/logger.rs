use chrono::{DateTime, Local};
use log::{Level, Log, Metadata, Record};

pub struct Logger;

impl Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let date: DateTime<Local> = Local::now();

            let date_str = date.format("%Y-%m-%d %H:%M").to_string();

            println!("[{}] [{}] ⇒ {}", date_str, record.level(), record.args());
        }
    }

    fn flush(&self) {}
}
