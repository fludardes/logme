# logme
**logme** — very basic logger implementation for Rust [log](https://github.com/rust-lang-nursery/log) crate. Currently only for usage in my personal projects, and not intended for other.
